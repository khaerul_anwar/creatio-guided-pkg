create or alter view IgloVwContactAgeDays
as

select Id as IgloId, Name as IgloName, BirthDate as IgloBirthDate,
datediff(day, BirthDate, getdate()) as IgloAgeDays
from Contact