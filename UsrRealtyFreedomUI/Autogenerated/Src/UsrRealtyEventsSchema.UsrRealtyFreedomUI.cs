﻿namespace Terrasoft.Configuration
{

	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Globalization;
	using Terrasoft.Common;
	using Terrasoft.Core;
	using Terrasoft.Core.Configuration;

	#region Class: UsrRealtyEventsSchema

	/// <exclude/>
	public class UsrRealtyEventsSchema : Terrasoft.Core.SourceCodeSchema
	{

		#region Constructors: Public

		public UsrRealtyEventsSchema(SourceCodeSchemaManager sourceCodeSchemaManager)
			: base(sourceCodeSchemaManager) {
		}

		public UsrRealtyEventsSchema(UsrRealtyEventsSchema source)
			: base( source) {
		}

		#endregion

		#region Methods: Protected

		protected override void InitializeProperties() {
			base.InitializeProperties();
			UId = new Guid("9fa02b2d-82cf-48e9-9723-fdf2992ba95a");
			Name = "UsrRealtyEvents";
			ParentSchemaUId = new Guid("50e3acc0-26fc-4237-a095-849a1d534bd3");
			CreatedInPackageId = new Guid("6038cec2-d1ae-4917-8bc5-888d8275a21f");
			ZipBody = new byte[] { 31,139,8,0,0,0,0,0,4,0,141,82,77,107,219,64,16,189,23,244,31,6,145,131,4,102,73,174,113,27,168,93,39,24,66,91,34,185,151,210,195,122,53,86,54,236,135,216,93,57,117,139,255,123,247,195,78,44,37,33,157,147,52,251,230,205,123,143,81,84,162,237,40,67,168,209,24,106,245,198,145,185,86,27,222,246,134,58,174,85,246,55,3,95,189,229,170,133,106,103,29,202,233,73,231,116,74,74,173,222,122,51,72,22,202,113,199,209,254,7,132,44,182,168,220,1,249,51,118,119,177,117,203,189,0,133,166,168,216,61,74,250,213,171,135,79,144,175,172,185,67,42,220,238,218,32,54,90,174,150,121,249,43,14,119,253,90,112,6,76,80,107,33,97,94,161,131,75,152,81,139,175,188,68,146,20,193,9,157,222,122,221,188,65,216,106,222,192,55,85,209,173,119,83,232,245,3,50,7,22,85,131,102,2,137,110,134,27,111,45,146,126,54,173,5,44,159,216,158,121,67,173,189,2,242,196,117,36,193,114,58,64,37,82,48,209,138,247,94,164,70,153,240,67,108,131,140,75,42,160,51,156,133,156,210,16,185,65,87,239,58,108,230,90,244,82,253,160,162,199,143,7,232,85,17,178,252,30,240,171,234,75,62,218,205,55,80,36,174,43,184,56,63,86,57,192,12,61,133,66,178,180,115,170,24,10,108,188,8,103,122,156,102,47,80,214,153,112,16,254,26,45,109,177,70,217,9,234,130,104,133,143,112,171,25,21,252,15,93,11,172,34,174,56,88,89,89,52,254,92,149,143,221,223,42,185,67,171,123,195,60,72,27,207,50,201,62,140,215,132,122,62,151,116,102,249,4,242,23,27,44,137,193,44,109,173,245,140,183,233,47,47,73,173,15,10,202,119,77,120,241,169,65,174,181,145,212,21,35,115,126,237,5,57,159,157,141,99,14,229,238,141,126,140,222,23,191,25,118,193,221,113,124,132,222,103,195,175,125,182,255,7,243,212,240,193,213,3,0,0 };
		}

		protected override void InitializeLocalizableStrings() {
			base.InitializeLocalizableStrings();
			SetLocalizableStringsDefInheritance();
			LocalizableStrings.Add(CreateValueIsTooBigLocalizableString());
		}

		protected virtual SchemaLocalizableString CreateValueIsTooBigLocalizableString() {
			SchemaLocalizableString localizableString = new SchemaLocalizableString() {
				UId = new Guid("9a522a0f-af84-5331-85ed-2350404fb706"),
				Name = "ValueIsTooBig",
				CreatedInPackageId = new Guid("6038cec2-d1ae-4917-8bc5-888d8275a21f"),
				CreatedInSchemaUId = new Guid("9fa02b2d-82cf-48e9-9723-fdf2992ba95a"),
				ModifiedInSchemaUId = new Guid("9fa02b2d-82cf-48e9-9723-fdf2992ba95a")
			};
			return localizableString;
		}

		#endregion

		#region Methods: Public

		public override void GetParentRealUIds(Collection<Guid> realUIds) {
			base.GetParentRealUIds(realUIds);
			realUIds.Add(new Guid("9fa02b2d-82cf-48e9-9723-fdf2992ba95a"));
		}

		#endregion

	}

	#endregion

}

