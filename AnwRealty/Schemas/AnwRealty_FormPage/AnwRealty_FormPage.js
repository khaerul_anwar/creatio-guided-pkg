define("AnwRealty_FormPage", /**SCHEMA_DEPS*/[]/**SCHEMA_DEPS*/, function/**SCHEMA_ARGS*/()/**SCHEMA_ARGS*/ {
	return {
		viewConfigDiff: /**SCHEMA_VIEW_CONFIG_DIFF*/[
			{
				"operation": "merge",
				"name": "CardToggleTabPanel",
				"values": {
					"styleType": "default",
					"bodyBackgroundColor": "primary-contrast-500",
					"selectedTabTitleColor": "auto",
					"tabTitleColor": "auto",
					"underlineSelectedTabColor": "auto",
					"headerBackgroundColor": "auto"
				}
			},
			{
				"operation": "merge",
				"name": "Feed",
				"values": {
					"dataSourceName": "PDS",
					"entitySchemaName": "AnwRealty"
				}
			},
			{
				"operation": "merge",
				"name": "AttachmentList",
				"values": {
					"columns": [
						{
							"id": "0a079371-b68a-4fc8-937e-aa8132435265",
							"code": "AttachmentListDS_Name",
							"caption": "#ResourceString(AttachmentListDS_Name)#",
							"dataValueType": 28,
							"width": 200
						}
					]
				}
			},
			{
				"operation": "insert",
				"name": "AnwName",
				"values": {
					"layoutConfig": {
						"column": 1,
						"row": 1,
						"colSpan": 1,
						"rowSpan": 1
					},
					"type": "crt.Input",
					"label": "$Resources.Strings.AnwName",
					"control": "$AnwName",
					"labelPosition": "auto",
					"multiline": false
				},
				"parentName": "SideAreaProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "AnwPriceUSD",
				"values": {
					"layoutConfig": {
						"column": 1,
						"row": 2,
						"colSpan": 1,
						"rowSpan": 1
					},
					"type": "crt.NumberInput",
					"label": "$Resources.Strings.NumberAttribute_84vzq0u",
					"labelPosition": "auto",
					"control": "$NumberAttribute_84vzq0u"
				},
				"parentName": "SideAreaProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "AnwArea",
				"values": {
					"layoutConfig": {
						"column": 1,
						"row": 3,
						"colSpan": 1,
						"rowSpan": 1
					},
					"type": "crt.NumberInput",
					"label": "$Resources.Strings.NumberAttribute_sdp6ku6",
					"labelPosition": "auto",
					"control": "$NumberAttribute_sdp6ku6"
				},
				"parentName": "SideAreaProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "AnwCommissionPercent",
				"values": {
					"layoutConfig": {
						"column": 1,
						"row": 4,
						"colSpan": 1,
						"rowSpan": 1
					},
					"type": "crt.NumberInput",
					"label": "$Resources.Strings.AnwRealtyOfferTypeAnwCommissionPercent",
					"labelPosition": "auto",
					"control": "$AnwRealtyOfferTypeAnwCommissionPercent",
					"visible": true,
					"readonly": true,
					"placeholder": "",
					"tooltip": ""
				},
				"parentName": "SideAreaProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "AnwCommissionUSD",
				"values": {
					"layoutConfig": {
						"column": 1,
						"row": 5,
						"colSpan": 1,
						"rowSpan": 1
					},
					"type": "crt.NumberInput",
					"label": "$Resources.Strings.NumberAttribute_du2ckhd",
					"labelPosition": "auto",
					"control": "$NumberAttribute_du2ckhd",
					"visible": true,
					"readonly": true,
					"placeholder": "",
					"tooltip": ""
				},
				"parentName": "SideAreaProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "AnwType",
				"values": {
					"layoutConfig": {
						"column": 1,
						"row": 1,
						"colSpan": 1,
						"rowSpan": 1
					},
					"type": "crt.ComboBox",
					"label": "$Resources.Strings.LookupAttribute_7g97ovr",
					"labelPosition": "auto",
					"control": "$LookupAttribute_7g97ovr",
					"listActions": [],
					"showValueAsLink": true,
					"controlActions": []
				},
				"parentName": "GeneralInfoTabContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "addRecord_pyh2igh",
				"values": {
					"code": "addRecord",
					"type": "crt.ComboboxSearchTextAction",
					"icon": "combobox-add-new",
					"caption": "#ResourceString(addRecord_pyh2igh_caption)#",
					"clicked": {
						"request": "crt.CreateRecordFromLookupRequest",
						"params": {}
					}
				},
				"parentName": "AnwType",
				"propertyName": "listActions",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "AnwRealtyOfferType",
				"values": {
					"layoutConfig": {
						"column": 2,
						"row": 1,
						"colSpan": 1,
						"rowSpan": 1
					},
					"type": "crt.ComboBox",
					"label": "$Resources.Strings.LookupAttribute_7tz4jlv",
					"labelPosition": "auto",
					"control": "$LookupAttribute_7tz4jlv",
					"listActions": [],
					"showValueAsLink": true,
					"controlActions": []
				},
				"parentName": "GeneralInfoTabContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "addRecord_6oqnvgd",
				"values": {
					"code": "addRecord",
					"type": "crt.ComboboxSearchTextAction",
					"icon": "combobox-add-new",
					"caption": "#ResourceString(addRecord_6oqnvgd_caption)#",
					"clicked": {
						"request": "crt.CreateRecordFromLookupRequest",
						"params": {}
					}
				},
				"parentName": "AnwRealtyOfferType",
				"propertyName": "listActions",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "AnwComment",
				"values": {
					"layoutConfig": {
						"column": 1,
						"row": 2,
						"colSpan": 1,
						"rowSpan": 1
					},
					"type": "crt.Input",
					"label": "$Resources.Strings.StringAttribute_167l0yg",
					"labelPosition": "auto",
					"control": "$StringAttribute_167l0yg",
					"multiline": false
				},
				"parentName": "GeneralInfoTabContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "ExpansionPanel_w8nuecv",
				"values": {
					"type": "crt.ExpansionPanel",
					"tools": [],
					"items": [],
					"title": "#ResourceString(ExpansionPanel_w8nuecv_title)#",
					"toggleType": "default",
					"togglePosition": "before",
					"expanded": true,
					"labelColor": "auto",
					"fullWidthHeader": false,
					"titleWidth": 20,
					"padding": {
						"top": "small",
						"bottom": "small",
						"left": "none",
						"right": "none"
					},
					"fitContent": true
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "GridContainer_iwlutap",
				"values": {
					"type": "crt.GridContainer",
					"rows": "minmax(max-content, 24px)",
					"columns": [
						"minmax(32px, 1fr)"
					],
					"gap": {
						"columnGap": "large",
						"rowGap": 0
					},
					"styles": {
						"overflow-x": "hidden"
					},
					"items": []
				},
				"parentName": "ExpansionPanel_w8nuecv",
				"propertyName": "tools",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "FlexContainer_nnzs6un",
				"values": {
					"type": "crt.FlexContainer",
					"direction": "row",
					"gap": "none",
					"alignItems": "center",
					"items": [],
					"layoutConfig": {
						"colSpan": 1,
						"column": 1,
						"row": 1,
						"rowSpan": 1
					}
				},
				"parentName": "GridContainer_iwlutap",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "GridDetailAddBtn_3lxcsjk",
				"values": {
					"type": "crt.Button",
					"caption": "#ResourceString(GridDetailAddBtn_3lxcsjk_caption)#",
					"icon": "add-button-icon",
					"iconPosition": "only-icon",
					"color": "default",
					"size": "medium",
					"clicked": {
						"request": "crt.CreateRecordRequest",
						"params": {
							"entityName": "AnwRealtyVisits",
							"defaultValues": [
								{
									"attributeName": "AnwRealtyId",
									"value": "$Id"
								}
							]
						}
					},
					"visible": true,
					"clickMode": "default"
				},
				"parentName": "FlexContainer_nnzs6un",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "GridDetailRefreshBtn_af3lr6g",
				"values": {
					"type": "crt.Button",
					"caption": "#ResourceString(GridDetailRefreshBtn_af3lr6g_caption)#",
					"icon": "reload-button-icon",
					"iconPosition": "only-icon",
					"color": "default",
					"size": "medium",
					"clicked": {
						"request": "crt.LoadDataRequest",
						"params": {
							"config": {
								"loadType": "reload"
							},
							"dataSourceName": "GridDetail_c8r4454DS"
						}
					}
				},
				"parentName": "FlexContainer_nnzs6un",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "GridDetailSettingsBtn_m4f3znp",
				"values": {
					"type": "crt.Button",
					"caption": "#ResourceString(GridDetailSettingsBtn_m4f3znp_caption)#",
					"icon": "actions-button-icon",
					"iconPosition": "only-icon",
					"color": "default",
					"size": "medium",
					"clickMode": "menu",
					"menuItems": []
				},
				"parentName": "FlexContainer_nnzs6un",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "GridDetailExportDataBtn_kbw08to",
				"values": {
					"type": "crt.MenuItem",
					"caption": "#ResourceString(GridDetailExportDataBtn_kbw08to_caption)#",
					"icon": "export-button-icon",
					"color": "default",
					"size": "medium",
					"clicked": {
						"request": "crt.ExportDataGridToExcelRequest",
						"params": {
							"viewName": "GridDetail_c8r4454"
						}
					}
				},
				"parentName": "GridDetailSettingsBtn_m4f3znp",
				"propertyName": "menuItems",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "GridDetailImportDataBtn_yaw3ba6",
				"values": {
					"type": "crt.MenuItem",
					"caption": "#ResourceString(GridDetailImportDataBtn_yaw3ba6_caption)#",
					"icon": "import-button-icon",
					"color": "default",
					"size": "medium",
					"clicked": {
						"request": "crt.ImportDataRequest",
						"params": {
							"entitySchemaName": "AnwRealtyVisits"
						}
					}
				},
				"parentName": "GridDetailSettingsBtn_m4f3znp",
				"propertyName": "menuItems",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "GridDetailSearchFilter_stdbch3",
				"values": {
					"type": "crt.SearchFilter",
					"placeholder": "#ResourceString(GridDetailSearchFilter_stdbch3_placeholder)#",
					"iconOnly": true,
					"targetAttributes": [
						"GridDetail_c8r4454"
					]
				},
				"parentName": "FlexContainer_nnzs6un",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "GridContainer_8854ob7",
				"values": {
					"type": "crt.GridContainer",
					"rows": "minmax(max-content, 32px)",
					"columns": [
						"minmax(32px, 1fr)",
						"minmax(32px, 1fr)"
					],
					"gap": {
						"columnGap": "large",
						"rowGap": 0
					},
					"styles": {
						"overflow-x": "hidden"
					},
					"items": []
				},
				"parentName": "ExpansionPanel_w8nuecv",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "AnwAutoAddedVisitsButton",
				"values": {
					"layoutConfig": {
						"column": 1,
						"row": 1,
						"colSpan": 1,
						"rowSpan": 1
					},
					"type": "crt.Button",
					"caption": "#ResourceString(Button_xkb1pny_caption)#",
					"color": "accent",
					"disabled": false,
					"size": "medium",
					"iconPosition": "only-text",
					"visible": true,
					"clicked": {
						"request": "crt.RunBusinessProcessRequest",
						"params": {
							"processName": "AnwButtonAutoAddRealtyVisitsProcess",
							"processRunType": "ForTheSelectedPage",
							"recordIdProcessParameterName": "UsrRealtyId"
						}
					},
					"clickMode": "default",
					"menuItems": []
				},
				"parentName": "GridContainer_8854ob7",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "GridDetail_c8r4454",
				"values": {
					"type": "crt.DataGrid",
					"layoutConfig": {
						"colSpan": 2,
						"column": 1,
						"row": 2,
						"rowSpan": 6
					},
					"features": {
						"rows": {
							"selection": {
								"enable": true,
								"multiple": true
							}
						}
					},
					"items": "$GridDetail_c8r4454",
					"visible": true,
					"fitContent": true,
					"primaryColumnName": "GridDetail_c8r4454DS_Id",
					"columns": [
						{
							"id": "311d8ad8-3931-fb36-80ec-fabe03fffb4d",
							"code": "GridDetail_c8r4454DS_AnwOwner",
							"path": "AnwOwner",
							"caption": "#ResourceString(GridDetail_c8r4454DS_AnwOwner)#",
							"dataValueType": 10,
							"referenceSchemaName": "Contact"
						},
						{
							"id": "51e8f0ba-13b4-e645-2e02-566337ea7888",
							"code": "GridDetail_c8r4454DS_AnwVisitDateTime",
							"path": "AnwVisitDateTime",
							"caption": "#ResourceString(GridDetail_c8r4454DS_AnwVisitDateTime)#",
							"dataValueType": 7
						},
						{
							"id": "09654f8c-7687-3e2c-49a9-17026527c18d",
							"code": "GridDetail_c8r4454DS_AnwComment",
							"caption": "#ResourceString(GridDetail_c8r4454DS_AnwComment)#",
							"dataValueType": 27
						}
					]
				},
				"parentName": "GridContainer_8854ob7",
				"propertyName": "items",
				"index": 1
			}
		]/**SCHEMA_VIEW_CONFIG_DIFF*/,
		viewModelConfigDiff: /**SCHEMA_VIEW_MODEL_CONFIG_DIFF*/[
			{
				"operation": "merge",
				"path": [
					"attributes"
				],
				"values": {
					"AnwName": {
						"modelConfig": {
							"path": "PDS.AnwName"
						}
					},
					"NumberAttribute_84vzq0u": {
						"modelConfig": {
							"path": "PDS.AnwPriceUSD"
						},
						"validators": {
							"MySuperValidator": {
								"type": "Anw.DGValidator",
								"params": {
									"minValue": 0,
									"message": "#ResourceString(PriceCannotBeLess)#"
								}
							}
						}
					},
					"NumberAttribute_sdp6ku6": {
						"modelConfig": {
							"path": "PDS.AnwArea"
						},
						"validators": {
							"MySuperValidator": {
								"type": "Anw.DGValidator",
								"params": {
									"minValue": 0,
									"message": "#ResourceString(AreaCannotBeLess)#"
								}
							}
						}
					},
					"LookupAttribute_7g97ovr": {
						"modelConfig": {
							"path": "PDS.AnwType"
						}
					},
					"LookupAttribute_7tz4jlv": {
						"modelConfig": {
							"path": "PDS.AnwRealtyOfferType"
						}
					},
					"StringAttribute_167l0yg": {
						"modelConfig": {
							"path": "PDS.AnwComment"
						},
						"validators": {
							"required": {
								"type": "crt.Required"
							}
						}
					},
					"GridDetail_c8r4454": {
						"isCollection": true,
						"modelConfig": {
							"path": "GridDetail_c8r4454DS",
							"sortingConfig": {
								"default": [
									{
										"direction": "asc",
										"columnName": "AnwOwner"
									}
								]
							}
						},
						"viewModelConfig": {
							"attributes": {
								"GridDetail_c8r4454DS_AnwOwner": {
									"modelConfig": {
										"path": "GridDetail_c8r4454DS.AnwOwner"
									}
								},
								"GridDetail_c8r4454DS_AnwVisitDateTime": {
									"modelConfig": {
										"path": "GridDetail_c8r4454DS.AnwVisitDateTime"
									}
								},
								"GridDetail_c8r4454DS_AnwComment": {
									"modelConfig": {
										"path": "GridDetail_c8r4454DS.AnwComment"
									}
								},
								"GridDetail_c8r4454DS_Id": {
									"modelConfig": {
										"path": "GridDetail_c8r4454DS.Id"
									}
								}
							}
						}
					},
					"NumberAttribute_du2ckhd": {
						"modelConfig": {
							"path": "PDS.AnwCommissionUSD"
						}
					},
					"AnwRealtyOfferTypeAnwCommissionPercent": {
						"modelConfig": {
							"path": "PDS.AnwRealtyOfferTypeAnwCommissionPercent"
						}
					}
				}
			},
			{
				"operation": "merge",
				"path": [
					"attributes",
					"Id",
					"modelConfig"
				],
				"values": {
					"path": "PDS.Id"
				}
			}
		]/**SCHEMA_VIEW_MODEL_CONFIG_DIFF*/,
		modelConfigDiff: /**SCHEMA_MODEL_CONFIG_DIFF*/[
			{
				"operation": "merge",
				"path": [],
				"values": {
					"primaryDataSourceName": "PDS",
					"dependencies": {
						"GridDetail_c8r4454DS": [
							{
								"attributePath": "AnwRealtyId",
								"relationPath": "PDS.Id"
							}
						]
					}
				}
			},
			{
				"operation": "merge",
				"path": [
					"dataSources"
				],
				"values": {
					"PDS": {
						"type": "crt.EntityDataSource",
						"config": {
							"entitySchemaName": "AnwRealty",
							"attributes": {
								"AnwRealtyOfferTypeAnwCommissionPercent": {
									"path": "AnwRealtyOfferType.AnwCommissionPercent",
									"type": "ForwardReference"
								}
							}
						},
						"scope": "page"
					},
					"GridDetail_c8r4454DS": {
						"type": "crt.EntityDataSource",
						"scope": "viewElement",
						"config": {
							"entitySchemaName": "AnwRealtyVisits",
							"attributes": {
								"AnwOwner": {
									"path": "AnwOwner"
								},
								"AnwVisitDateTime": {
									"path": "AnwVisitDateTime"
								},
								"AnwComment": {
									"path": "AnwComment"
								}
							}
						}
					}
				}
			}
		]/**SCHEMA_MODEL_CONFIG_DIFF*/,
		handlers: /**SCHEMA_HANDLERS*/[
			{
				request: "crt.HandleViewModelAttributeChangeRequest",
				/* The custom implementation of the system query handler. */
				handler: async (request, next) => {
					/* If the AnwPriceUSD field changes, take the following steps. */
					if (request.attributeName === 'NumberAttribute_84vzq0u') {
						var price = await request.$context.NumberAttribute_84vzq0u;
						if(price > 50000){
							request.$context.enableAttributeValidator('StringAttribute_167l0yg', 'required');
						}else{
							request.$context.disableAttributeValidator('StringAttribute_167l0yg', 'required');
						}
					}
					/* Call the next handler if it exists and return its result. */
					return next?.handle(request);
				}
			},
			{
				request: "crt.HandleViewModelAttributeChangeRequest",
				/* The custom implementation of the system query handler. */
				handler: async (request, next) => {
					/* If the AnwPriceUSD field changes, take the following steps. */
					if (request.attributeName === 'NumberAttribute_84vzq0u' || // if price changed
					   request.attributeName === 'AnwRealtyOfferTypeAnwCommissionPercent' ) {//or multiplier changed
						var price = await request.$context.NumberAttribute_84vzq0u;
						var percent = await request.$context.AnwRealtyOfferTypeAnwCommissionPercent;
						var commission = price * percent / 100;
						request.$context.NumberAttribute_du2ckhd = commission;
						}
					/* Call the next handler if it exists and return its result. */
					return next?.handle(request);
				}
			}
		]/**SCHEMA_HANDLERS*/,
		converters: /**SCHEMA_CONVERTERS*/{}/**SCHEMA_CONVERTERS*/,
		validators: /**SCHEMA_VALIDATORS*/{
			"Anw.DGValidator": {
				validator: function (config) {
					return function (control) {
						let value = control.value;
						let minValue = config.minValue;
						let valueIsCorrect = value > minValue;
						var result;
						if (valueIsCorrect) {
							result = null;
						} else {
							result = {
								"Anw.DGValidator": { 
									message: config.message
								}
							};
						}
						return result;
					};
				},
				params: [
					{
						name: "minValue"
					},
					{
						name: "message"
					}
				],
				async: false
			}
		}/**SCHEMA_VALIDATORS*/
	};
});